<?php 

if (isset($this->session->userdata['email'])) {

 //redirect('admin/dashboard');
}
else
{
     redirect('admin/login');
}

?>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12" style="width: 33.3%;">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-home"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total Companies</span>
                  <span class="info-box-number"><?php if(isset($totalpmc)){echo $totalpmc;}?><small></small></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12" style="width: 33.3%;">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-user-plus"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total Tenants</span>
                  <span class="info-box-number"><?php if(isset($totlaTenants)){echo $totlaTenants;}?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12" style="width: 33.3%;">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-usd"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total Revenue</span>
                  <span class="info-box-number">$<?php if(isset($totalRevenue)){echo  number_format((float) $totalRevenue, 2, '.', '');}?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
<!--            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">New Members</span>
                  <span class="info-box-number">2,000</span>
                </div> /.info-box-content 
              </div> /.info-box 
            </div> /.col -->
          </div><!-- /.row -->
 <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
              <!-- MAP & BOX PANE -->
            <!-- TABLE: LATEST ORDERS -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">PMCs Latest Five Applications </h3>
<!--                    <select name="all" id="all" class="showAllData">
                        <option value="">--Select--</option>
                        <option value="1">--Show  All--</option>-->
                    </select>
                     </div>
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Sr. No.</th>  
                          <th>PMC Name</th>
                          <th>Address</th>
                          <th>Email</th>
                          <th>PMC Type</th>
                          <th>Application Date</th>
                          <th>Executive Name</th>
                          <th>Contact No.</th>
                        </tr>
                      </thead>
                      <tbody>
                         <?php 
                        // print_r($latest_app);
                          $i=1;
                         if(!empty($latest_app))
                         {
                            
                             $type='';
                             foreach($latest_app as $value)
                             {
                                 if(!empty($value['pmc_type'])) {
                                    $type=getPMCType($value['pmc_type']);
                                 }
                                 /*
                                 if($value['pmc_type']=='1')
                                 {
                                     $type='Residential';
                                     
                                 }
                                 if($value['pmc_type']=='2')
                                 {
                                     $type='Multi Unit';
                                     
                                 }
                                  if($value['pmc_type']=='3')
                                 {
                                     $type='Both (Residential/Multi Unit)';
                                     
                                 }*/
                         ?>
                        <tr>
                           <td><?php echo $i;?></td>  
                          <td><a href="<?php echo base_url();?>propertyapplication/approve_application/<?php echo $value['pmc_id']; ?>"><?php echo ucfirst($value['company_name']); ?></a></td>
                            <td><?php echo ucfirst($value['address']); ?></td>
                            <td><?php echo $value['email']; ?></td>
                           <td><?php echo $type; ?></td>
                           <td><?php echo date('m/d/Y',strtotime($value['application_date'])); ?></td>
                           <td><?php echo ucfirst($value['account_executive_name']); ?></td>
                           <td><?php echo ucfirst($value['contact']); ?></td>
                        </tr>
                         <?php  $i++;}
                         
                          } 
                            if($i==1)
                         {?>
                          <tr>
                              <td style="text-align: center">   No records  found</td>
                          </tr>
                           <?php
                         }
                          ?> 
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
<!--                <div class="box-footer clearfix">
                  <a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                  <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                </div> /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
        
            <!-- *********** -->


<div class="col-md-12">
              <!-- MAP & BOX PANE -->
            <!-- TABLE: LATEST ORDERS -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Monthly Revenue Graph </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="">
                   <div class="box-body">
                        <div style="margin:20px auto; text-align:center;">
                          <div id="curve_chart" style="width: 1050px; height: 500px"></div>
                        </div>
		</div> 
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div><!-- /.col -->        
 <!--     ****** Graph********* -->
    </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      var customArr =[];
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
     
      function drawChart() {
          var the_data = [[]];
		  //var i=0;
		  var month=["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
		  
          var url="<?php echo base_url().'admin/lineGraph'; ?> ";
                   $.ajax({
                       url:url,
                       type:'POST',
                       dataType: 'text',
                       success:function(msg)
                       {
                         customArr=msg.split(',');
                          customArr = customArr.map(Number);
                  // alert(customArr);
						the_data[0]=['Month','Revenue'];
						var current_date = new Date();
						//alert(current_date);
                        var current_month = current_date.getMonth()+1;
						//alert(current_month);
					  for(i=1; i<=12;i++){
						  var month_data = [];
						  if((i)<=current_month){
						  if(customArr[i-1] >= 0)
						  {
								the_data[i] = [new Date(current_date.getFullYear(),[i-1]),customArr[i-1]];					  
						  }
					  }else{
					  break;}
					  }
					 
					 
                         // ['Revenue', 'Month'],
						 
                         // ['Jan',  customArr[0] ],
                         // ['Feb',  customArr[1] ],
                         // ['Mar',  customArr[2]],
                         // ['Apr',  customArr[3]],
                         // ['May',  customArr[4]],
                         // ['Jun',  customArr[5]],
                         // ['Jul',  customArr[6]],
                         // ['Aug',  customArr[7]],
                         // ['Sep',  customArr[8]],
                         // ['Oct',  customArr[9]],
                         // ['Nov',  customArr[10]],
                         // ['Dec',  customArr[11]] 
					 	//alert(the_data);
					var data = new google.visualization.arrayToDataTable(the_data);
                     

                        //var data = google.visualization.arrayToDataTable([['Revenue', 'Month'],['Feb',23424]]);          
          var options = {
//          title: 'sdfsdf',
//          curveType: 'function',
//          legend: { position: 'bottom' }
        };
         var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
         chart.draw(data, options);
                      
                        }
                         
      }); 
                         
      }
    </script>

